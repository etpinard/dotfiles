#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

if ! "${HERE}/../bin/_amiontermux"; then
    echo "Hmm. Does not seem to be running on termux?"
    echo "Skip."
    exit 0
fi

mkdir -p ~/.termux

echo 'Link termux.properties'
pushd ~/.termux > /dev/null
ln -fs "$HERE"/termux.properties termux.properties
popd > /dev/null
