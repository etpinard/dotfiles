# Vim tips (that I don't use often enough to remember each time)

## Indent multiple lines

Mostly from https://stackoverflow.com/a/5212123

```
>>    Indent line by shiftwidth spaces
<<    De-indent line by shiftwidth spaces
5>>   Indent 5 lines
5==   Re-indent 5 lines

>%    Increase indent of a braced or bracketed block (place cursor on brace first)
=%    Reindent a braced or bracketed block (cursor on brace)
<%    Decrease indent of a braced or bracketed block (cursor on brace)
]p    Paste text, aligning indentation with surroundings

=i{   Re-indent the 'inner block', i.e. the contents of the block
=a{   Re-indent 'a block', i.e. block and containing braces
=2a{  Re-indent '2 blocks', i.e. this block and containing block

>i{   Increase inner block indent
<i{   Decrease inner block indent

gg=G  Re-indent entire buffer

Vjj>  [visual mode] Visually mark and then indent three lines
```

## Add characters to the end of each line

Initially taken from https://stackoverflow.com/a/596291, for some reason I
always think just `A` in the block-selection suffices.

- block-select (`<c-v>`) corresponding lines
- then `$A <character>`

## Folds

```
zf  creates a fold from visual selection
za  toggle a fold at the cursor.
zo  opens a fold at the cursor.
zO  opens all folds at the cursor.
zc  closes a fold under cursor.
zM  closes all open folds.
zd  deletes the fold at the cursor.
zE  deletes all folds.
```

## Annex

```
# to rename linked file, you must "unlock" them first
git annex unlock <file>
```
