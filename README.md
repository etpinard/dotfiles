# dotfiles

Personal configuration files

## Installation

```
(dotfiles) $ chmod +x _setup.sh

# minimal setup 'required' for every machine I used
(dotfiles) $ ./_setup.sh

# submodules one-by-one
(dotfiles) $ ./_setup.sh vim bin bash
```

## Tests

Test bash syntax with [`shellcheck`](https://www.shellcheck.net/)

```
$ sudo apt install shellcheck
(dotfiles) $ ./_test.sh
```

## Test 24-bit colours

I started using some fancy colorschemes in `nvim`, getting them to work
in `tmux` started inside `alacritty` is a little tricky.

Luckily [`@andersevenrud`](https://github.com/andersevenrud) worked out the details in this
[gist](https://gist.github.com/andersevenrud/015e61af2fd264371032763d4ed965b6).

The required patches are included in the `tmux.conf` and `alacritty.toml`.

To test, run `./_test-term-colors.sh` in `tmux` from `alacritty`. You should
see "continuous" color gradients.

Please note, that it's not sufficient to source your `tmux.conf` after making them
patches, you need to kill the session and start a new one.
Thanks [`@elanmed`](https://github.com/ElanMedoff)!

## License

[MIT](./LICENSE)
