#! /bin/bash

EXIT_CODE=0

# NOTE returns `0` when no matches
shellcheck _test.sh || EXIT_CODE=$?
shellcheck _test-term-colors.sh || EXIT_CODE=$?
shellcheck bash/bashrc || EXIT_CODE=$?
shellcheck bash/bash_aliases || EXIT_CODE=$?
shellcheck bash/profile || EXIT_CODE=$?
git ls-files --exclude='_setup.sh' --ignored -c -z | xargs -0r shellcheck || EXIT_CODE=$?
git ls-files --exclude='bin/_*' --ignored -c -z | xargs -0r shellcheck || EXIT_CODE=$?
git ls-files --exclude='*.rc' --ignored -c -z | xargs -0r shellcheck || EXIT_CODE=$?

# check for trailing spaces at EOLs
# NOTE returns `0` when 1 or matches
git --no-pager grep --full-name -I -n -e ' $' . && EXIT_CODE=1
# check for tab characters
# NOTE returns `0` when 1 or matches
git --no-pager grep --full-name -I -n -P '\t' . && EXIT_CODE=1

exit $EXIT_CODE
