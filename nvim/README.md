# Neovim

Now split from `vim`, with a config written in lua!

## Installation

### Step 0: install deps

- `luarocks`

### Step 1: install `bob`

_an `nvim` version manager_

```sh
cd ~/bin
wget https://github.com/MordechaiHadad/bob/releases/download/{version}/bob-linux-x86_64.zip
unzip bob-linux-x86_64.zip
chmod +x bob-linux-x86_64/bob
ln -fs bob-linux-x86_64/bob bob

# then
bob install stable
bob use stable
```

### Step 2: test 24-bit colors!

_to support fancier nvim colorschemes_

```sh
cd $HERE/..
./_test-term-colors.sh
```

should display a "continuous" color gradient.

NOTE: this currently requires patches in the `tmux` and `alacritty` configs.

### Step 3: run `./_setup.sh`

```sh
cd $HERE
./_setup.sh
```
