-- spelling stuff

vim.opt.spell = true
vim.opt.spelllang = { "en_ca" }

-- one badly-written three-way spelling toggle function!
local function toggle_spelling()
  local opts = { scope = "local" }

  local onoff = vim.api.nvim_get_option_value("spell", opts)
  local lang = vim.api.nvim_get_option_value("spelllang", opts)
  local msg = "Set spelling to: "

  if not onoff then
    vim.api.nvim_set_option_value("spell", true, opts)
    vim.api.nvim_set_option_value("spelllang", "en_ca", opts)
    msg = msg .. "en_ca"
  elseif lang == "en_ca" then
    vim.api.nvim_set_option_value("spell", true, opts)
    vim.api.nvim_set_option_value("spelllang", "fr", opts)
    msg = msg .. "fr"
  elseif lang == "fr" then
    vim.api.nvim_set_option_value("spell", false, opts)
    msg = msg .. "OFF"
  else
    vim.api.nvim_set_option_value("spell", true, opts)
    vim.api.nvim_set_option_value("spelllang", "en_ca", opts)
    msg = msg .. "en_ca"
  end

  vim.notify(msg)
end

-- NOTE I use `<super><space` to toggle keyboard language,
-- so this keymap here feels very natural
vim.keymap.set("n", "<Leader><space>", toggle_spelling, { desc = "Toggle spelling" })
