-- Personal additions

vim.keymap.set("n", "<Leader>l", ":set number!<CR>", { desc = "Toggle line numbers" })

vim.keymap.set("n", "<c-j>", "M<c-d>", { desc = "Move down one chunk" })
vim.keymap.set("n", "<c-k>", "M<c-u>", { desc = "Move up one chunk" })

-- NOTE `<Leader>e` lists the buffers in Telescope
vim.keymap.set("n", "]e", ":bnext<CR>", { desc = "Go to next buffer" })
vim.keymap.set("n", "[e", ":bprev<CR>", { desc = "Go to prev buffer" })

-- taken from: https://vi.stackexchange.com/a/3881
vim.keymap.set(
  "n",
  "<Leader>j",
  ":<C-u>call append(line('.'),   repeat([''], v:count1))<CR>",
  { desc = "Add line below" }
)
vim.keymap.set(
  "n",
  "<Leader>k",
  ":<C-u>call append(line('.')-1, repeat([''], v:count1))<CR>",
  { desc = "Add line below" }
)
