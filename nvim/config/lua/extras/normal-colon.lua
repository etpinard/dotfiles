-- Taken from https://github.com/edte/normal-colon.nvim
-- minus the code I do not use

function AlphaGotoNext(cmd, always)
  always = always or 0
  if not vim.g.after_alpha_goto_do and always == 0 then
    return ":"
  end

  vim.g.after_alpha_goto_do = 2
  vim.g.after_alpha_goto_time = vim.fn.reltimefloat(vim.fn.reltime())
  return cmd
end

vim.api.nvim_set_keymap("n", ";", "v:lua.AlphaGotoNext(';')", { expr = true })
vim.api.nvim_set_keymap("x", ";", "v:lua.AlphaGotoNext(';')", { expr = true })
