return {
  "nvim-telescope/telescope.nvim",
  event = "VimEnter",
  branch = "0.1.x",
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    local actions = require("telescope.actions")
    require("telescope").setup({
      defaults = {
        mappings = {
          i = { ["<esc>"] = actions.close },
        },
        prompt_prefix = "> ",
      },
    })

    local builtin = require("telescope.builtin")

    vim.keymap.set("n", "<Leader>p", builtin.find_files, { desc = "Telescope find files" })
    vim.keymap.set("n", "<Leader>o", builtin.oldfiles, { desc = "Telescope find recent files" })
    vim.keymap.set("n", "<Leader>e", builtin.buffers, { desc = "Telescope buffers" })
    vim.keymap.set("n", "<Leader>r", builtin.registers, { desc = "Telescope registers" })
    vim.keymap.set("n", "<Leader>z", builtin.spell_suggest, { desc = "Telescope selling suggestions" })
    vim.keymap.set("n", "<Leader>a", builtin.grep_string, { desc = "Telescope Grep current word" })

    vim.keymap.set("n", "<Leader>aa", builtin.live_grep, { desc = "Telescope Grep" })
  end,
}
