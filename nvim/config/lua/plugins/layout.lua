return {
  {
    "nvim-lualine/lualine.nvim",
    config = function()
      -- remove mode from line below statusline
      vim.opt.showmode = false
      -- then set the lualine options
      require("lualine").setup({
        options = {
          -- not a fancy icon person!
          icons_enabled = false,
        },
      })
    end,
  },
  {
    "akinsho/bufferline.nvim",
    config = function()
      -- required for bufferline
      vim.opt.termguicolors = true
      -- then set the bufferline options
      require("bufferline").setup({
        options = {
          -- not a fancy icon person!
          show_buffer_icons = false,
        },
      })
    end,
  },
  {
    "echasnovski/mini.files",
    config = function()
      require("mini.files").setup()
      -- NOTE open with this and close with `q`
      vim.keymap.set("n", "<Leader>.", "<cmd>lua MiniFiles.open()<CR>", { desc = "Show file explorer" })
    end,
  },
}
