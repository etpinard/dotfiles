-- takes care of the `set hlsearch` dance
return {
  "romainl/vim-cool",
  event = { "BufReadPost", "BufNewFile" },
  config = function()
    -- Case-insensitive searching
    -- UNLESS \C or one or more capital letters in the search term
    vim.opt.ignorecase = true
    vim.opt.smartcase = true
  end,
}
