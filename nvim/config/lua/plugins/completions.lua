-- inspiration from:
--
-- https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings#confirm-candidate-on-tab-immediately-when-theres-only-one-completion-entry
-- https://github.com/fredrikekre/.dotfiles/blob/c1e4547b2111da1cd5148d9cf17b79d3d4310806/.config/nvim/lua/plugins/cmp.lua

local function format(entry, vim_item)
  local source_mapping = {
    nvim_lsp = "[LSP]",
    luasnip = "[snp]",
    path = "[pth]",
    buffer = "[buf]",
    spell = "[spl]",
    latex_symbols = "[tex]",
    cmdline = "[cmd]",
  }
  vim_item.menu = source_mapping[entry.source.name]
  return vim_item
end

local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local function cmp_config(cmp)
  local luasnip = require("luasnip")
  -- load snippets from `friendly-snippets`
  require("luasnip.loaders.from_vscode").lazy_load()
  luasnip.config.setup({})

  cmp.setup({
    snippet = {
      expand = function(args)
        luasnip.lsp_expand(args.body)
      end,
    },
    completion = {
      keyword_length = 3,
      completeopt = "menu,menuone,noinsert",
    },
    mapping = cmp.mapping.preset.insert({
      ["<CR>"] = cmp.mapping.confirm({ select = true }),

      ["<Tab>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          if #cmp.get_entries() == 1 then
            cmp.confirm({ select = true })
          else
            cmp.select_next_item()
          end
        elseif luasnip.locally_jumpable(1) then
          luasnip.jump(1)
        elseif has_words_before() then
          cmp.complete()
          if #cmp.get_entries() == 1 then
            cmp.confirm({ select = true })
          end
        else
          fallback()
        end
      end, { "i", "s" }),

      ["<S-Tab>"] = cmp.mapping.select_prev_item(),
    }),
    sources = {
      {
        name = "lazydev",
        -- set group index to 0 to skip loading LuaLS completions as lazydev recommends it
        group_index = 0,
      },
      { name = "nvim_lsp", score = 1 },
      { name = "luasnip", score = 1 },
      { name = "path", score = 1 },
      {
        name = "buffer",
        score = 2,
        option = {
          get_bufnrs = function()
            return vim.api.nvim_list_bufs()
          end,
        },
      },
      {
        name = "spell",
        score = 0,
        option = {
          -- the default `true` is annoying with latex symbols completion
          preselect_correct_word = false,
        },
      },
      {
        name = "latex_symbols",
        score = 10,
        -- show the command and insert the symbol, great for writing Julia
        -- TODO adapt for writing LaTeX
        option = { strategy = 0 },
      },
    },
    formatting = {
      format = format,
    },
  })
end

local function cmp_config_cmdline(cmp)
  cmp.setup.cmdline({ "/", "?" }, {
    completion = {
      keyword_length = 1,
    },
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = "buffer" },
    },
    formatting = {
      format = format,
    },
  })

  cmp.setup.cmdline(":", {
    completion = {
      keyword_length = 3,
    },
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = "path" },
    }, {
      { name = "cmdline" },
    }),
    matching = { disallow_symbol_nonprefix_matching = false },
    formatting = {
      format = format,
    },
  })
end

return {
  {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lua",
      {
        "L3MON4D3/LuaSnip",
        dependencies = { "rafamadriz/friendly-snippets" },
      },
      "saadparwaiz1/cmp_luasnip",
      "f3fora/cmp-spell",
      "kdheepak/cmp-latex-symbols",
    },
    config = function()
      local cmp = require("cmp")
      cmp_config(cmp)
      cmp_config_cmdline(cmp)
    end,
  },
}
