return {
  -- simple comment toggler!
  "echasnovski/mini.comment",
  opts = {
    mappings = {
      comment = "",
      comment_line = "<leader>c",
      comment_visual = "<leader>c",
    },
  },
}
