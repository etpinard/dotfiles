return {
  "catppuccin/nvim",
  name = "catppuccin",
  priority = 1000,
  config = function()
    vim.opt.termguicolors = true

    require("catppuccin").setup({
      -- more ideas: https://github.com/catppuccin/nvim/discussions/323
      color_overrides = {
        mocha = {
          base = "#000000",
          mantle = "#000000",
          crust = "#000000",
        },
      },
    })

    vim.cmd("colorscheme catppuccin-mocha")
  end,
}
