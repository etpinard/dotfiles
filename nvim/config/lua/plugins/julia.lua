return {
  {
    "JuliaEditorSupport/julia-vim",
    config = function()
      -- this is the indent style I like in Julia
      -- TODO implement this style for other languages
      -- when done, we may not need `julia-vim` anymore
      vim.g.julia_indent_align_brackets = 1
      vim.g.julia_indent_align_funcargs = 1
      -- disable <Tab> completion,
      -- use `cmp-latex-symbols` instead
      vim.g.latex_to_unicode_tab = "off"
    end,
  },
}
