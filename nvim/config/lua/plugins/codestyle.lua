return {
  {
    -- trim whitespaces and empty first/last lines on save
    "cappyzawa/trim.nvim",
    opts = {},
  },

  -- to install dependencies for `conform.nvim` and `nvim-lint`
  { "williamboman/mason.nvim" },

  {
    -- full list of available formatters:
    -- https://github.com/stevearc/conform.nvim?tab=readme-ov-file#formatters
    "stevearc/conform.nvim",
    event = { "BufWritePre" },
    cmd = { "ConformInfo" },
    keys = {
      {
        "<Leader>f",
        function()
          require("conform").format({ async = true })
        end,
        mode = "",
        desc = "Format buffer",
      },
    },
    opts = {
      formatters_by_ft = {
        lua = { "stylua" },
        -- python = {}
        -- julia = {}
      },
      default_format_opts = {
        lsp_format = "fallback",
      },
      -- do not format on save
      -- format_on_save = {}
    },
  },
  {
    "zapling/mason-conform.nvim",
    config = function()
      require("mason-conform").setup({
        ensure_installed = { "stylua" },
      })
    end,
  },

  {
    -- full list of available linters:
    -- https://github.com/mfussenegger/nvim-lint?tab=readme-ov-file#available-linters
    "mfussenegger/nvim-lint",
    config = function()
      local lint = require("lint")

      lint.linter_by_ft = {
        markdown = { "vale" },
      }

      vim.api.nvim_create_autocmd({ "BufWritePost" }, {
        callback = function()
          require("lint").try_lint()
        end,
      })
    end,
  },
  -- FIXME I can't really get this to work unfortunately
  -- {
  --   "rshkarin/mason-nvim-lint",
  --   config = function()
  --     require('mason-nvim-lint').setup({
  --       ensure_installed = { "vale" }
  --     })
  --   end
  -- }
}
