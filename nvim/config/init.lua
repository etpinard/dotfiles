-- Set leader key (has to be set before any plugins are loaded)
vim.g.mapleader = "\\"
vim.g.maplocalleader = "\\"

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
end
vim.opt.rtp:prepend(lazypath)

-- Import extras lua logic from `./lua/extras/`
require("extras/from-kickstart")
require("extras/normal-colon")
require("extras/restore-cursor-position")
require("extras/spelling")
require("extras/personal")

-- Setup plugins from the `./lua/plugins/<topic>` files
local lazy = require("lazy")
lazy.setup("plugins")
