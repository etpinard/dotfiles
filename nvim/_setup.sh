#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

mkdir -p ~/.config/nvim

echo 'Link bob.rc in ~/bin/rc'
mkdir -p ~/bin/rc
pushd ~/bin/rc > /dev/null
ln -fs "$HERE"/bob.rc bob.rc
popd > /dev/null

echo 'Link init.lua'
pushd ~/.config/nvim > /dev/null
ln -fs "$HERE"/config/init.lua init.lua
popd > /dev/null

echo 'Link lua directory'
pushd ~/.config/nvim > /dev/null
ln -fs "$HERE"/config/lua lua
popd > /dev/null
