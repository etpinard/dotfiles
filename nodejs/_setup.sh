#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

echo 'Link nvm.rc in ~/bin/rc'
mkdir -p ~/bin/rc
pushd ~/bin/rc > /dev/null
ln -fs "$HERE"/nvm.rc nvm.rc
popd > /dev/null

echo 'Install global packages'
npm install -g http-server
