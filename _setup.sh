#! /bin/bash -e

EXIT_CODE=0
setupfilename="_setup.sh"

# the submodules that should 'always' get installed
declare -a ALWAYS=("bin" "bash" "ssh" "git" "vim" "tmux")

# all the submodules, just for validation
declare -a ALL=("bin" "bash" "ssh" "git" "vim" "tmux" "julia" "gnome" "termux" "alacritty" "mpv" "nodejs" "python" "rust")

_isvalid () {
    for sub2 in "${ALL[@]}"; do
        if [[ "$1" == "$sub2" ]]; then
            return 0
        fi
    done
    return 1
}

_main () {
    submodules=("$@")
    for sub in "${submodules[@]}"; do
        if _isvalid "$sub"; then
            echo 'Setting up:' "$sub"
            pushd "$sub" > /dev/null
                chmod +x "$setupfilename"
                # shellcheck disable=SC1090
                . "$setupfilename"
            popd > /dev/null
        else
            echo '[ERR] unrecognized submodules' "$sub"
            EXIT_CODE=1
        fi
        echo ''
    done
}

_usage () {
cat << EOF
    Usage: @etpinard/dotfiles/_setup.sh

    Setup (mostly "ln -s") personal dotfiles !

    # minimal setup 'required' for every machine I used
    - ./_setup.sh

    # target specic application (e.g. julia)
    - ./_setup.sh julia

EOF
}

case $1 in
    --help | -h)
        _usage
        exit 0
        ;;
    ALL | all)
        _main "${ALL[@]}"
        ;;
    *)
        if [ $# -eq 0 ]; then
            _main "${ALWAYS[@]}"
        else
            _main "$@"
        fi
        ;;
esac

exit $EXIT_CODE
