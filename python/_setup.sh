#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

echo 'Link pyenv.rc in ~/bin/rc'
mkdir -p ~/bin/rc
pushd ~/bin/rc > /dev/null
ln -fs "$HERE"/pyenv.rc pyenv.rc
popd > /dev/null
