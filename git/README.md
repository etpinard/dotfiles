# git

Personal git config file.

## gui

The alias for `git g` opens [`git-gui`](https://git-scm.com/docs/git-gui) if
available (i.e. locally) and fallbacks to
[`lazygit`](https://github.com/jesseduffield/lazygit) (on remote machines I
used).

To install `git-gui`:

```
apt install git-gui
```

To install `lazygit`, easiest to download the `Linux` tarball on:

https://github.com/jesseduffield/lazygit/releases

## annex

The config include a few aliases for
[`git-annex`](https://git-annex.branchable.com/) commands, to install it:

```
apt install git-annex
```
