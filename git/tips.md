# Git tips (that I don't use often enough to remember each time)

## `--ours` / `--theirs`

taken from <https://nitaym.github.io/ourstheirs/> - big shoutout!

```
$ git checkout main
$ git merge feature
Auto-merging Document
CONFLICT (content): Merge conflict in codefile.js
Automatic merge failed; fix conflicts and then commit the result.

# to select the changes done in main
$ git checkout --ours codefile.js

# to select the changes done in feature
$ git checkout --theirs codefile.js

# then
$ git add codefile.js
$ git merge --continue
[main 5d01884] Merge branch 'feature'
```

```
$ git checkout feature
$ git rebase main
First, rewinding head to replay your work on top of it...
Applying: a commit done in branch feature
error: Failed to merge in the changes.
...

# to select the changes done in main
$ git checkout --ours codefile.js

# to select the changes done in feature
$ git checkout --theirs codefile.js

# then
$ git add codefile.js
$ git rebase --continue
Applying: a commit done in branch feature
```

## Other articles

s/o Julia Evans <https://social.jvns.ca/@b0rk>

- <https://jvns.ca/blog/2023/11/06/rebasing-what-can-go-wrong-/>
- <https://jvns.ca/blog/2023/11/10/how-cherry-pick-and-revert-work/>
