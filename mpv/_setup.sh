#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")
DIR=$HOME/.config/mpv/

echo 'Link mpv.conf'
mkdir -p "$DIR"
pushd "$DIR" > /dev/null
ln -fs "$HERE"/mpv.conf mpv.conf
popd > /dev/null
