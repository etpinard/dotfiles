#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

mkdir -p ~/bin

for f in "$HERE"/*; do
    bf="$(basename "$f")"
    if [ "$bf" == '_setup.sh' ] || [ "$bf" == 'README.md' ]; then
        continue
    fi

    echo 'Link' "$f" 'in ~/bin'
    pushd ~/bin > /dev/null
    ln -fs "$f" "$bf"
    popd > /dev/null
done
