#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

echo 'Setup .julia/config/ directory'
mkdir -p ~/.julia/config

echo 'Link startup.jl'
pushd ~/.julia/config > /dev/null
ln -fs "$HERE"/startup.jl startup.jl
popd > /dev/null

echo 'Install startup.jl deps'
if [ -x "$(command -v julia)" ]; then
    julia "$HERE"/init_global_env.jl
else
    echo 'julia not in path, skip step'
fi

echo 'Link juliaup.rc in ~/bin/rc'
mkdir -p ~/bin/rc
pushd ~/bin/rc > /dev/null
ln -fs "$HERE"/juliaup.rc juliaup.rc
popd > /dev/null
