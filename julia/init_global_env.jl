import Pkg

Pkg.add(["Revise",
         "Debugger", "Infiltrator",
         "BenchmarkTools",
         "Pluto"])

# Potential additions:
# - https://github.com/ronisbr/TerminalPager.jl
# - https://github.com/tshort/Eyeball.jl
# - https://github.com/KristofferC/OhMyREPL.jl
# - https://julialogging.github.io/TerminalLoggers.jl/stable/
# - https://github.com/aviatesk/JET.jl
# - https://github.com/JuliaDebug/Cthulhu.jl
# - https://github.com/MichaelHatherly/InteractiveErrors.jl
# - https://github.com/BioTurboNick/AbbreviatedStackTraces.jl
# - https://github.com/jw3126/WhyNotEqual.jl
# - https://github.com/JuliaTesting/TestEnv.jl
# - https://github.com/JuliaLang/AllocCheck.jl
# - https://github.com/LilithHafner/BasicAutoloads.jl
# - https://github.com/LilithHafner/Chairmarks.jl (au lieu de BenchmarkTools?)
#
# MORE:
# - https://discourse.julialang.org/t/code-block-only-for-packagecompiler-and-not-for-revise/99870/5
