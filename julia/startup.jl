#=

Julia startup sequence:
1) load `Pkg`
2) try loading "global" `Revise`
3) try activating env corresponding to `./Project.toml`
4) try loading "env" `Revise`
5) print messages

To not load startup file, use:
$ julia --startup-file=no

=#

function __take_io_str(fn)
    io = IOBuffer()
    fn(io)
    return String(take!(io))
end

try
    import Pkg
catch e
    @error "Error while loading Pkg" exception=(e, catch_backtrace())
end

const msg_global_revise_status::String = try
    using Revise
    __take_io_str() do io
        Pkg.status("Revise"; io)
    end
catch e
    @error "Error while loading _global_ Revise" exception=(e, catch_backtrace())
    ""
end

const msg_activate_env::String = try
    if isfile("Project.toml")
        __take_io_str() do io
            Pkg.activate("."; io)
        end
    else
        ""
    end
catch e
    @warn "Error while activating environment" exception=(e, catch_backtrace())
    ""
end

const msg_env_revise_status::String = try
    using Revise
    __take_io_str() do io
        Pkg.status("Revise"; io)
    end
catch e
    @error "Error while loading _env_ Revise" exception=(e, catch_backtrace())
    ""
end

atreplinit() do repl
    if !isempty(msg_activate_env)
        msg = split(msg_activate_env, "\n")[end-1]
        @info "Project.toml found!" msg
    end

    in_env_wo_revise = (!isempty(msg_activate_env) &&
                        occursin("No Matches", msg_env_revise_status) &&
                        !isempty(msg_global_revise_status))
    not_in_env = (isempty(msg_activate_env) &&
                  !isempty(msg_global_revise_status))
    in_env_with_revise = (!isempty(msg_activate_env) &&
                          occursin("Revise", msg_env_revise_status) &&
                          !isempty(msg_env_revise_status))

    if in_env_wo_revise || not_in_env
        status = split(msg_global_revise_status, "\n")[end-1]
        @info "Revise loaded from global Julia env" status
    elseif in_env_with_revise
        pieces = split(msg_global_revise_status, "\n")
        msg = pieces[1]
        status = pieces[end-1]
        @info "Revise loaded from `Project.toml` env" msg status
    else
        @warn "Something went wrong during `startup.jl`"
    end
end

# add temporary environment for each session, so that e.g
#
# julia> using Plots
# │ Package Plots not found, but a package named Plots is available from a registry.
# │ Install package?
# │   (confab) pkg> add Plots
# └ (y/n/o) [y]: o
#
# shows a temporary environment as option
#
# taken from:
# https://discourse.julialang.org/t/tip-macro-to-install-use-package-in-temporary-environment/103190/21
insert!(LOAD_PATH, 2, mktempdir())
