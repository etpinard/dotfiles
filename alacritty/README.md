# alacritty

Personal alacritty config file, now as a TOML for alacritty > v0.13

## Theme

I'm currently using [catppuccin-mocha](https://github.com/catppuccin/alacritty) theme,
with a `#000000` black background. The theme is installed during `./_setup.sh`.

## [JuliaMono font](https://juliamono.netlify.app/)

```
mkdir -p .local/share/font/JuliaMono
cd !$

wget https://github.com/cormullion/juliamono/releases/download/v<X.Y>/JuliaMono-ttf.tar.gz
untar JuliaMono.tar.gz
rm JuliaMono.tar.gz

fc-cache -f -v
fc-list | grep JuliaMono
```
