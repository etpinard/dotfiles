#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")
DIR=$HOME/.config/alacritty

echo 'Make alacritty config dir'
mkdir -p "$DIR"

echo 'Download theme'
pushd "$DIR" > /dev/null
curl -LO https://github.com/catppuccin/alacritty/raw/main/catppuccin-mocha.toml
popd > /dev/null

echo 'Link alacritty.toml'
pushd "$DIR" > /dev/null
ln -fs "$HERE"/alacritty.toml alacritty.toml
popd > /dev/null
