#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

echo 'Link rustup.rc in ~/bin/rc'
mkdir -p ~/bin/rc
pushd ~/bin/rc > /dev/null
ln -fs "$HERE"/rustup.rc rustup.rc
popd > /dev/null
